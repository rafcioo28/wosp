"""empty message

Revision ID: f2e468f951a7
Revises: 0e01e1313cb4
Create Date: 2020-12-28 14:39:08.689224

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f2e468f951a7'
down_revision = '0e01e1313cb4'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('puszka', sa.Column('wydana', sa.Integer(), server_default='0', nullable=False))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('puszka', 'wydana')
    # ### end Alembic commands ###
