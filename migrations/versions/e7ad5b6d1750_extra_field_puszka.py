"""extra field - puszka

Revision ID: e7ad5b6d1750
Revises: 1902ffc68b88
Create Date: 2021-12-27 11:07:53.450794

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e7ad5b6d1750'
down_revision = '1902ffc68b88'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('puszka', sa.Column('extra_field', sa.String(length=600), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('puszka', 'extra_field')
    # ### end Alembic commands ###
