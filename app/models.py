from app import db, login
from flask_login import UserMixin
from sqlalchemy.sql import expression, func
from werkzeug.security import generate_password_hash, check_password_hash


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    super_user = db.Column(
        db.Boolean, server_default=expression.false(), nullable=False)
    inactive_user = db.Column(
        db.Boolean, server_default=expression.false(), nullable=False)
    wydanie_puszki = db.Column(
        db.Boolean, server_default=expression.false(), nullable=False)
    puszki = db.relationship('Puszka', backref='user', lazy='dynamic')

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def get_sum(self):
        sumy_wpisow = self.puszki.all()
        return sum(x.get_sum() for x in sumy_wpisow)

    def __repr__(self):
        return f'User: {self.username}'


class Puszka(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    numer = db.Column(db.String(64), index=True, unique=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    zatwierdzona = db.Column(
        db.Boolean, server_default=expression.false(), nullable=False)
    wydana = db.Column(db.Integer, nullable=False, server_default="0")
    pozycje = db.relationship(
        'PozycjaPuszki', backref='puszka', lazy='dynamic')
    extra_field = db.Column(db.String(600), nullable=True)

    def get_sum(self):
        lista_wpisow = self.pozycje.all()
        return sum(x.get_sum() for x in lista_wpisow)

    def get_list(self):
        return db.session.query(
            PozycjaPuszki.nominal,
            func.sum(PozycjaPuszki.liczba)
        ).\
            group_by(PozycjaPuszki.nominal).\
            filter(PozycjaPuszki.puszka_id == self.id).all()

    def __repr__(self):
        return f'Puszka nr. {self.numer}'


class PozycjaPuszki(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nominal = db.Column(db.Float(10, 2), nullable=False)
    liczba = db.Column(db.Integer, nullable=True)
    puszka_id = db.Column(db.Integer, db.ForeignKey('puszka.id'))

    def get_sum(self):
        return self.nominal * self.liczba
