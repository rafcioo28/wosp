from app import app, db
from app.models import User, Puszka, PozycjaPuszki
from app.forms import LoginForm, RegistrationForm, ChangeUserName, ChangePass
from app.forms import DodajPuszke, DodajPozycjePuszki, ZmienPuszke, WierszPuszki
from flask import flash, render_template, redirect, url_for, request
from flask_login import current_user, login_user, logout_user, login_required


@app.route('/')
def index():
    puszki = Puszka.query.filter_by(zatwierdzona=True)
    licznik = sum(x.get_sum() for x in puszki)
    if current_user.is_authenticated:
        return render_template('front_log.html', title='Licznik', licznik=licznik)
    return render_template('front.html', title='Licznik', licznik=licznik)


@app.route('/counter')
def counter():
    puszki = Puszka.query.filter_by(zatwierdzona=True)
    licznik = sum(x.get_sum() for x in puszki)
    if current_user.is_authenticated:
        return render_template('front_log.html', title='Licznik', licznik=licznik)
    return render_template('counter.html', title='Licznik', licznik=licznik)


@app.route('/log', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Nieprawidłowy nazwa użytkownika lub hasło')
            return redirect(url_for('login'))
        login_user(user)
        print(current_user.inactive_user)
        if (not current_user.inactive_user):
            return redirect(url_for('index'))
        else:
            logout_user()
    return render_template(
        'login.html',
        title='Logowanie do systemu',
        form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/nowy', methods=['GET', 'POST'])
@login_required
def register():
    if not current_user.super_user:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        if User.query.filter_by(username=form.username.data).first():
            flash(f'Użytkownik {form.username.data} już istnieje.')
            return redirect(url_for('register'))
        user = User(username=form.username.data,
                    super_user=form.super_user.data,
                    wydanie_puszki=form.wydanie_puszki.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash(f'Użytkownik {form.username.data} został dodany.')
        return redirect(url_for('register'))
    return render_template(
        'register.html', title="Nowy użytkownik", form=form)


@app.route('/lista_uzytkownikow')
@login_required
def user_list():
    if not current_user.super_user:
        return redirect(url_for('index'))
    users = User.query.all()
    return render_template(
        'users_list.html', title='Lista użytkownikow', users=users)


@app.route('/user/<username>', methods=['GET', 'POST'])
@login_required
def user(username):
    if not current_user.super_user:
        return redirect(url_for('index'))
    user = User.query.filter_by(username=username).first_or_404()
    form = ChangeUserName()
    # print(form.validate_on_submit())
    if form.validate_on_submit():
        user_test = User.query.filter_by(username=form.username.data).first()
        if user_test and user != user_test:
            flash('Użytkownik już istnieje.')
            return redirect(url_for('user', username=user.username))
        user.username = form.username.data
        user.super_user = form.super_user.data
        user.inactive_user = form.inactive_user.data
        user.wydanie_puszki = form.wydanie_puszki.data
        if form.password2.data:
            user.set_password(form.password2.data)
        db.session.add(user)
        db.session.commit()
        if current_user.super_user:
            return redirect(url_for('user_list'))
    elif request.method == 'GET':
        form.username.data = user.username
        form.super_user.data = user.super_user
        form.inactive_user.data = user.inactive_user
        form.wydanie_puszki.data = user.wydanie_puszki

    return render_template('user.html', title='Edycja użytkownika', form=form)


@app.route('/pass/<username>', methods=['GET', 'POST'])
@login_required
def user_pass(username):
    if current_user.username != username:
        return redirect(url_for('index'))

    form = ChangePass()
    if form.validate_on_submit():
        if not current_user.check_password(form.old_pass.data):
            flash('Stare hasło jest niepoprawne')
            return redirect(url_for(
                'user_pass', username=current_user.username))
        current_user.set_password(form.password.data)
        db.session.commit()
        flash('Hasło zostało zmienione')
    return render_template('user_pass.html', title='Zmiana hasłoa', form=form)


# Lista puszek włsnych użytkownika
@app.route('/puszki')
@login_required
def puszki():
    puszki = current_user.puszki.filter(
        Puszka.wydana == 2).order_by(Puszka.id.desc()).all()

    return render_template(
        'puszki_lista.html',
        title='Lista puszek',
        puszki=puszki)


# Lista puszek do wydania
@app.route('/puszki_wydanie', methods=['GET', 'POST'])
@login_required
def puszki_wydanie():
    form = DodajPuszke()
    puszki = Puszka.query.filter(Puszka.wydana != 2).all()

    if form.validate_on_submit():
        puszka_test = Puszka.query.filter_by(numer=form.numer.data).first()
        if puszka_test:
            flash('Puszka już istnieje.')
            return redirect(url_for('puszki_wydanie'))
        puszka = Puszka(numer=form.numer.data, user=current_user)
        db.session.add(puszka)
        db.session.commit()
        flash(f'Puszka nr {puszka.numer} dodana do listy.')
        return redirect(url_for('puszki_wydanie'))

    return render_template(
        'puszki_wydanie.html',
        title='Lista puszek',
        puszki=puszki, form=form)


# Wydanie puszki funkcja wywołana przycisku wydania
@app.route('/wydana_puszka/<id>/<opcja>', methods=['GET', 'POST'])
@login_required
def wydana_puszka(id, opcja):
    puszka = Puszka.query.get(id)
    if (opcja == "1"):
        puszka.wydana = 1
        msg = f'Puszka nr {puszka.numer} wydana wolontariuszowi.'
        page = url_for('puszki_wydanie')
    if (opcja == "2"):
        puszka.wydana = 2
        puszka.user = current_user
        msg = f'Puszka nr {puszka.numer} Przyjęta do liczenia.'
        page = url_for('puszka', id=puszka.id)
    db.session.add(puszka)
    db.session.commit()
    flash(msg)
    return redirect(page)


@app.route('/puszka/<id>', methods=['GET', 'POST'])
@login_required
def puszka(id):
    puszka = Puszka.query.get(id)
    pozycje = puszka.pozycje.order_by(PozycjaPuszki.id.desc()).all()
    form = DodajPozycjePuszki()
    wiersz_form = WierszPuszki()
    nazwa_puszki = ZmienPuszke(liczacy=puszka.user.id, extra_field=puszka.extra_field)
    nazwa_puszki.liczacy.choices = [(user.id, user.username) for user in User.query.all()]

    # zatwierdzenie puszki, sprawdzenie przycisku zatwierdź
    if nazwa_puszki.zatwierdz.data:
        if puszka.zatwierdzona and current_user.super_user:
            puszka.zatwierdzona = False
            db.session.commit()
        else:
            puszka.zatwierdzona = True
            db.session.commit()
        return redirect(url_for('puszka', id=id))

    # Wykrycie przycisku przy zmianie nazwy puszki i wykonanie zmiany
    if nazwa_puszki.validate_on_submit() and nazwa_puszki.zmien_numer.data:
        puszka.numer = nazwa_puszki.numer.data
        puszka.user = User.query.get(nazwa_puszki.liczacy.data)
        db.session.commit()
        return redirect(url_for('puszka', id=id))

    # Dodanie extra informacji do puszki
    if nazwa_puszki.validate_on_submit() and nazwa_puszki.extra_save.data:
        puszka.extra_field = nazwa_puszki.extra_field.data.strip()
        db.session.commit()
        return redirect(url_for('puszka', id=id))

    # dodanie wiersza z nominałem i liczbą do puszki
    if form.validate_on_submit() and form.submit.data:
        pozycja = PozycjaPuszki()
        pozycja.nominal = form.nominal.data
        if form.liczba.data < 0:
            return redirect(url_for('puszka', id=id))
        pozycja.liczba = form.liczba.data
        pozycja.puszka = puszka

        db.session.add(pozycja)
        db.session.commit()
        return redirect(url_for('puszka', id=id))

    # aktualizacja wiersza puszki
    if wiersz_form.validate_on_submit() and wiersz_form.zapisz_wiersz.data:
        wiersz = PozycjaPuszki.query.get(wiersz_form.id.data)
        wiersz.liczba = 0 if wiersz_form.liczba.data < 0 else form.liczba.data
        db.session.commit()
        return redirect(url_for('puszka', id=id))

    # zmaina napisu na przycisku Zatwierdź/Otwórz
    nazwa_puszki.numer.data = puszka.numer
    if puszka.zatwierdzona:
        nazwa_puszki.zatwierdz.label.text = 'Otwórz'

    # gdy puszka zatwierdzona edytować może tylko super user, gdy zwykły user wyświetlana
    # templatka bez możliwości edycji
    if puszka.zatwierdzona and not current_user.super_user:
        return render_template(
            'puszka_zatwierdzona.html', title='Zatwierdzona puszka',
            form=form, puszka=puszka, nazwa_puszki=nazwa_puszki, pozycje=pozycje, wiersz_form=wiersz_form)

    return render_template(
        'puszka.html', title='Liczenie puszki',
        form=form, puszka=puszka, nazwa_puszki=nazwa_puszki, pozycje=pozycje, wiersz_form=wiersz_form)


# usunięcie pozycji puszki 
@app.route('/puszka_pozycja/<id>/<pozycja>', methods=['GET', 'POST'])
@login_required
def pozycja_usun(id, pozycja):
    pozycja_puszki = PozycjaPuszki.query.get(pozycja)
    db.session.delete(pozycja_puszki)
    db.session.commit()
    return redirect(url_for('puszka', id=id))


# wydruk podziękowania dla wolontariusza
@app.route('/puszka/<id>/wydruk', methods=['GET', 'POST'])
@login_required
def drukuj_puszke(id):
    puszka = Puszka.query.get(id)
    pozycje = puszka.get_list()
    return render_template('wydruk.html', title='Podziękowania za zbiórkę', puszka=puszka, pozycje=pozycje)

# wydruk wydania puszki
@app.route('/puszka/<id>/wydruk_wydanie', methods=['GET', 'POST'])
@login_required
def drukuj_puszke_wydanie(id):
    puszka = Puszka.query.get(id)
    pozycje = puszka.get_list()
    return render_template('wydruk_wydanie.html', title='Podziękowania za zbiórkę', puszka=puszka, pozycje=pozycje)


# wydruk uprosczony puszki
@app.route('/puszka/<id>/wydruk_prosty')
@login_required
def drukuj_puszke_prosty(id):
    puszka = Puszka.query.get(id)
    pozycje = puszka.get_list()
    return render_template('wydruk_prosty.html', title='Wydruk puszki', puszka=puszka)



@app.route('/lista_puszek', methods=['GET', 'POST'])
@login_required
def lista_puszek():
    if not current_user.super_user:
        return redirect(url_for('index'))

    puszki = Puszka.query.order_by(Puszka.numer.asc()).all()

    return render_template('wszystkie_puszki.html', title='Wszystki puszki', puszki=puszki)


@app.route('/user_puszki/<id>', methods=['GET', 'POST'])
@login_required
def user_lista_puszek(id):
    if not current_user.super_user:
        return redirect(url_for('index'))
    user = User.query.get(int(id))
    print(user)
    puszki = user.puszki.order_by(Puszka.numer.asc()).all()

    return render_template('user_puszki.html', title='Wszystki puszki', puszki=puszki, user=user)
