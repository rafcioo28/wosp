from wtforms.fields.simple import TextAreaField
from app.models import User, Puszka
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms import HiddenField, IntegerField, SelectField
from wtforms.validators import DataRequired, ValidationError, EqualTo, InputRequired
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from flask_sqlalchemy import SQLAlchemy


class LoginForm(FlaskForm):
    username = StringField('Użytkownik', validators=[DataRequired()])
    password = PasswordField('Hasło', validators=[DataRequired()])
    submit = SubmitField('Logowanie')


class RegistrationForm(FlaskForm):
    username = StringField('Użytkownik', validators=[DataRequired()])
    super_user = BooleanField('Super użytkownik')
    inactive_user = BooleanField('Użytkownik zablokowany')
    wydanie_puszki = BooleanField('Wydawanie puszek')
    password = PasswordField('Hasło', validators=[DataRequired()])
    password2 = PasswordField(
        'Powtórz hasło',
        validators=[DataRequired(), EqualTo(
            'password', 'hasła nie są zgodne')])
    submit = SubmitField('Zapisz')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Użytkownik już istnieje.')


class ChangeUserName(FlaskForm):
    username = StringField('Użytkownik', validators=[DataRequired()])
    super_user = BooleanField('Super użytkownik')
    inactive_user = BooleanField('Użytkownik zablokowany')
    wydanie_puszki = BooleanField('Wydawanie puszek')
    password = PasswordField('Hasło')
    password2 = PasswordField(
        'Powtórz hasło',
        validators=[EqualTo(
            'password', 'hasła nie są zgodne')])
    submit = SubmitField('Zapisz')


class ChangePass(FlaskForm):
    username = HiddenField('Użytkownik')
    old_pass = PasswordField('Stare hasło', validators=[DataRequired()])
    password = PasswordField('Hasło', validators=[DataRequired()])
    password2 = PasswordField(
        'Powtórz hasło',
        validators=[DataRequired(), EqualTo(
            'password', 'hasła nie są zgodne')])
    submit = SubmitField('Zapisz')


class DodajPuszke(FlaskForm):
    numer = StringField('Numer', validators=[DataRequired('Popraw nr puszki')])
    submit = SubmitField('Dodaj')


class ZmienPuszke(FlaskForm):
    numer = StringField('Numer puszki', validators=[
                        DataRequired('Popraw nr puszki')])
    zatwierdzona = BooleanField('Zatwierdź')
    extra_field = TextAreaField('Dodatkowe informacje')
    extra_save = SubmitField('Zapisz')
    liczacy = SelectField('Liczący', coerce=int)
    zmien_numer = SubmitField('Zapisz')
    zatwierdz = SubmitField('Zatwierdż')


class WierszPuszki(FlaskForm):
    id = HiddenField("ID wiersza")
    liczba = IntegerField('Liczba szt.', validators=[InputRequired()])
    zapisz_wiersz = SubmitField('Zapisz')


class DodajPozycjePuszki(FlaskForm):
    nominal = SelectField(
        'Nominał ',
        choices=[
            ('0.01', '1gr'),
            ('0.02', '2gr'),
            ('0.05', '5gr'),
            ('0.1', '10gr'),
            ('0.2', '20gr'),
            ('0.5', '50gr'),
            ('1', '1zł'),
            ('2', '2zł'),
            ('5', '5zł'),
            ('10', '10zł'),
            ('20', '20zł'),
            ('50', '50zł'),
            ('100', '100zł'),
            ('200', '200zł'),
            ('500', '500zł'),
        ]
    )
    liczba = IntegerField('Liczba szt.', validators=[DataRequired(
        'Wprowadź poprawną wartość w nominale ')])
    submit = SubmitField('Dodaj')
